﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace CS481_HW5
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Maps : ContentPage
    {
        public Maps()
        {
            InitializeComponent();
            var mapInit = MapSpan.FromCenterAndRadius(new Position(33.135243, -117.125337), Distance.FromMiles(7));

            //set map view location
            sd.MoveToRegion(mapInit);


            //create list of pins for picker view
            List<Pin> pins = new List<Pin>();

           //Places to wait in long lines during the TP Panic 2020
            pins.Add(new Pin
            {
                Type = PinType.Place,
                Position = new Position(33.130768, -117.062940),
                Label = "Walmart Escondido",
                Address = "1330 E Grand Ave, Escondido, CA 9202 ",
            });

            pins.Add(new Pin
            {
                Type = PinType.Place,
                Position = new Position(33.136197, -117.123423),
                Label = "Walmart San Marcos",
                Address = "732 Center Dr, San Marcos, CA 92069"
            });

            pins.Add(new Pin
            {
                Type = PinType.Place,
                Position = new Position(33.135243, -117.125337),
                Label = "Costco SanMarcos",
                Address = "725 Center Dr, San Marcos, CA 92069"
            });
            pins.Add(new Pin
            {
                Type = PinType.Place,
                Position = new Position(33.175240, -117.217225),
                Label = "Walmart Vista",
                Address = "1800 University Dr, Vista, CA 92083"
            });

            //manual add map pins
            sd.Pins.Add(pins.ElementAt(0));
            sd.Pins.Add(pins.ElementAt(1));
            sd.Pins.Add(pins.ElementAt(2));
            sd.Pins.Add(pins.ElementAt(3));

            // Add pins to picker
            Locations.ItemsSource = pins;


        }





        //MapType View changer event handlers for toolbar buttons
        public void StreetView(object sender, EventArgs e)
        {
            //change view to street map
            sd.MapType = MapType.Street;
        }
        public void SatView(object sender, EventArgs e)
        {
            //change view to satellite map
            sd.MapType = MapType.Satellite;
        }
        public void HybridView(object sender, EventArgs e)
        {

            //change view to hybrid map
            sd.MapType = MapType.Hybrid;
        }

        //move map to pin's gps coordinates
        private void Locations_Selected(object sender, EventArgs e)
        {

            var lat = ((Pin)Locations.SelectedItem).Position.Latitude;
            var lon= ((Pin)Locations.SelectedItem).Position.Longitude;
            var marker = MapSpan.FromCenterAndRadius(new Position(lat,lon), Distance.FromMiles(1));

            sd.MoveToRegion(marker);


        }
    
}
}