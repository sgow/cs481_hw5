﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW5
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new Maps());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}

/*
 Functioning Map = 10 pts
 done

Street View = 10 pts
done
Satellite View =  10 pts
done


Pins (<=4) = 12 pts
done

Picker (for pins) = 8 pts

Picker should take you to Pin location on map!

Pins must be locations unique/special to you, do not include the school.

Picker on same page as Map = +1 pt
done
Comments = +1 pt
done
Incremental Commits = +3 pts
done
*/